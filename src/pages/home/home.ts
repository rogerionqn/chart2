import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import * as HighCharts from 'highcharts';
import * as $ from 'jquery';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    

  constructor(public navCtrl: NavController, private alertController: AlertController) {}
data = [];
 inter;
 series = [];
  
  
  public pausar(int: boolean, int2: boolean){
    let flag = int;
    let flag2 = int2;
    console.log(flag);
    var self = this;
   
    if(flag){
    
    HighCharts.chart('container', {
        chart: {
            type: 'spline',
            animation: HighCharts.svg, // don't animate in old IE
            marginRight: 10,
            zoomType: 'x',
            panning: true,
            
            events: {
                load: function () {
                   
                    
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    
        self.inter =  setInterval(function () {
                console.log(self.inter);
               
                        var x = (new Date()).getTime(), // current time
                        y = Math.random();
                        series.addPoint([x, y], true, true);
                        console.log('passou');
                      

                        
                }, 1000);
                },
        
                
              }
              
            
        },
       
      
        time: {
            useUTC: false
        },
      
        title: {
            text: 'testes'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Random data',
            data: (function () {

               if(flag2){
                    // generate an array of random data
                    var
                        time = (new Date()).getTime(),
                        i;
        
                    for (i = -1; i <= 0; i += 1) {
                        self.data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return self.data;
                }else{
                   // return self.data = [];
                //   self.data.fill(null);
                 //  return self.data;
                }
               
            }())
        }]
      });
    }
    else{
        clearInterval(self.inter);
    }     
        
        

  }
 
  

}
